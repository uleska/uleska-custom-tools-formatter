import xml.etree.ElementTree as ET


def save_vulns_to_file(vulns, file_location, version="1.0"):
    """
    Converts vulnerabilities to xml and put it on the disk
    :param vulns: List of the vulnerability model
    :param file_location: location where to save file
    :param version: version of xml schema
    """
    if version == "1.1":
        tree = __make_element_tree_version_1_1__(vulns)
    elif version == "1.2":
        tree = __make_element_tree_version_1_2__(vulns)
    else:
        tree = __make_element_tree_version_1__(vulns)
    tree.write(file_location, "UTF-8")


def print_vulns(vulns, version="1.0"):
    """
    Converts vulnerabilities to xml and returns a string
    :param vulns: List of the vulnerability model
    :param version: version of xml schema
    :return: str in xml format
    """
    if version == "1.1":
        tree = __make_element_tree_version_1_1__(vulns)
    elif version == "1.2":
        tree = __make_element_tree_version_1_2__(vulns)
    else:
        tree = __make_element_tree_version_1__(vulns)
    return ET.tostring(tree.getroot(), encoding="UTF-8", method="xml")


def __make_element_tree_version_1__(vulns):
    root = ET.Element("custom_tool", attrib={"schemaVersion": "1.0"})
    vulns_element = ET.SubElement(root, "vulnerabilities")
    for vuln in vulns:
        __make_element_for_vulnerability__(vuln, vulns_element)
    return ET.ElementTree(root)


def __make_element_tree_version_1_1__(vulns):
    root = ET.Element("custom_tool", attrib={"schemaVersion": "1.1"})
    vulns_element = ET.SubElement(root, "vulnerabilities")
    for vuln in vulns:
        __make_element_for_vulnerability_1_1__(vuln, vulns_element)
    return ET.ElementTree(root)


def __make_element_tree_version_1_2__(vulns):
    root = ET.Element("custom_tool", attrib={"schemaVersion": "1.2"})
    vulns_element = ET.SubElement(root, "vulnerabilities")
    for vuln in vulns:
        vuln_element = __make_element_for_vulnerability_1_1__(vuln, vulns_element)
        if vuln.md5 is not None:
            md5_element = ET.SubElement(vuln_element, "md5")
            md5_element.text = vuln.md5
    return ET.ElementTree(root)


def __make_element_for_vulnerability__(vuln, vulns_element):
    vuln_element = ET.SubElement(
        vulns_element, "vulnerability", attrib={"title": vuln.title}
    )
    summary_element = ET.SubElement(vuln_element, "summary")
    summary_element.text = vuln.summary
    explanation_element = ET.SubElement(vuln_element, "explanation")
    explanation_element.text = vuln.explanation
    recommendation_element = ET.SubElement(vuln_element, "recommendation")
    recommendation_element.text = vuln.recommendation
    source_element = ET.SubElement(vuln_element, "source")
    source_element.text = vuln.source
    severity_element = ET.SubElement(vuln_element, "severity")
    severity_element.text = vuln.severity.value
    return vuln_element


def __make_element_for_vulnerability_1_1__(vuln, vulns_element):
    vuln_element = __make_element_for_vulnerability__(vuln, vulns_element)
    if vuln.cvss is not None:
        cvss_element = ET.SubElement(vuln_element, "cvss")
        cvss_element.text = vuln.cvss
    if vuln.cve is not None:
        cve_element = ET.SubElement(vuln_element, "cve")
        cve_element.text = vuln.cve
    if vuln.cwe is not None:
        cwe_element = ET.SubElement(vuln_element, "cwe")
        cwe_element.text = vuln.cwe
    return vuln_element
