from setuptools import setup

setup(
    name="uleska-custom-tools-formatter",
    version="1.2.0",
    description="Takes a vulnerability model and converts it into the correct uleska custom tools format",
    author="Uleska",
    author_email="developers@uleska.com",
    license="MIT License",
    zip_safe=False,
    keywords="uleska",
    packages=["uleska"],
)
