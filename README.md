# uleska-custom-tools-formatter

Small lib that takes a vulnerability model and converts it into the correct uleska custom tools format.


# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2020-08-18
### Added
- Vulnerability can now have a CVSS of version 3 or higher [see](https://www.first.org/cvss/)
- Vulnerability can now have a CVE [see](https://cve.mitre.org/)
- Vulnerability can now have a CWE [see](https://cwe.mitre.org/)

## [1.2.0] - 2021-01-11
### Added
- Vulnerability can now have an [md5 value](https://www.lifewire.com/what-is-md5-2625937). This can be used by custom tool developers to identify duplicate vulnerabilities where the default duplication implementation is ineffective.