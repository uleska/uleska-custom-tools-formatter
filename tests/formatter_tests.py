from uleska.formatter import save_vulns_to_file
from uleska.vulnerability import Vulnerability, Severity
from unittest import TestCase


class FormatterTests(TestCase):
    def test_saves_xml_with_one_vuln(self):
        # given
        vuln = Vulnerability(
            "sql injection",
            "can do sql injection",
            "sql injection is bad",
            "fix it",
            "index.html",
            Severity.HIGH,
        )
        expected_result = """<custom_tool schemaVersion="1.0">
<vulnerabilities>
<vulnerability title="sql injection">
<summary>can do sql injection</summary>
<explanation>sql injection is bad</explanation>
<recommendation>fix it</recommendation>
<source>index.html</source>
<severity>HIGH</severity>
</vulnerability>
</vulnerabilities>
</custom_tool>"""
        expected_result = expected_result.replace("\n", "")

        # when
        save_vulns_to_file([vuln], "/tmp/test.xml", "1.0")

        # then
        with open("/tmp/test.xml") as xml:
            read_data = xml.read()
            self.assertEqual(read_data, expected_result)

    def test_saves_xml_with_many_vulns(self):
        # given
        vuln1 = Vulnerability(
            "sql injection",
            "can do sql injection",
            "sql injection is bad",
            "fix it",
            "index.html",
            Severity.HIGH,
        )
        vuln2 = Vulnerability(
            "xss", "can do xss", "xss is bad", "fix it", "users.html", Severity.MEDIUM
        )
        vuln3 = Vulnerability(
            "missing header",
            "missing a secuirty header",
            "tell the browser to be more secure",
            "fix it",
            "login.html",
            Severity.LOW,
        )
        expected_result = """<custom_tool schemaVersion="1.0">
<vulnerabilities>
<vulnerability title="sql injection">
<summary>can do sql injection</summary>
<explanation>sql injection is bad</explanation>
<recommendation>fix it</recommendation>
<source>index.html</source>
<severity>HIGH</severity>
</vulnerability>
<vulnerability title="xss">
<summary>can do xss</summary>
<explanation>xss is bad</explanation>
<recommendation>fix it</recommendation>
<source>users.html</source>
<severity>MEDIUM</severity>
</vulnerability>
<vulnerability title="missing header">
<summary>missing a secuirty header</summary>
<explanation>tell the browser to be more secure</explanation>
<recommendation>fix it</recommendation>
<source>login.html</source>
<severity>LOW</severity>
</vulnerability>
</vulnerabilities>
</custom_tool>"""
        expected_result = expected_result.replace("\n", "")

        # when
        save_vulns_to_file([vuln1, vuln2, vuln3], "/tmp/test.xml", "1.0")

        # then
        with open("/tmp/test.xml") as xml:
            read_data = xml.read()
            self.assertEqual(read_data, expected_result)
