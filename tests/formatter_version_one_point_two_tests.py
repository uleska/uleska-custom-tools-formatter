from uleska.formatter import save_vulns_to_file
from uleska.vulnerability import Vulnerability, Severity
from unittest import TestCase


class FormatterTests(TestCase):
    def test_saves_xml_with_one_vuln_and_md5(self):
        # given
        vuln = Vulnerability(
            "sql injection",
            "can do sql injection",
            "sql injection is bad",
            "fix it",
            "index.html",
            Severity.HIGH,
            md5="1a79a4d60de6718e8e5b326e338ae533",
        )
        expected_result = """<custom_tool schemaVersion="1.2">
<vulnerabilities>
<vulnerability title="sql injection">
<summary>can do sql injection</summary>
<explanation>sql injection is bad</explanation>
<recommendation>fix it</recommendation>
<source>index.html</source>
<severity>HIGH</severity>
<md5>1a79a4d60de6718e8e5b326e338ae533</md5>
</vulnerability>
</vulnerabilities>
</custom_tool>"""
        expected_result = expected_result.replace("\n", "")

        # when
        save_vulns_to_file([vuln], "/tmp/test.xml", "1.2")

        # then
        with open("/tmp/test.xml") as xml:
            read_data = xml.read()
            self.assertEqual(read_data, expected_result)

    def test_saves_xml_with_many_vulns_with_md5(self):
        # given
        vuln1 = Vulnerability(
            "sql injection",
            "can do sql injection",
            "sql injection is bad",
            "fix it",
            "index.html",
            Severity.HIGH,
            md5="1a79a4d60de6718e8e5b326e338ae533",
        )
        vuln2 = Vulnerability(
            "xss",
            "can do xss",
            "xss is bad",
            "fix it",
            "users.html",
            Severity.MEDIUM,
            md5="1a79a4d60de6718e8e5b326e338ae533",
        )
        expected_result = """<custom_tool schemaVersion="1.2">
<vulnerabilities>
<vulnerability title="sql injection">
<summary>can do sql injection</summary>
<explanation>sql injection is bad</explanation>
<recommendation>fix it</recommendation>
<source>index.html</source>
<severity>HIGH</severity>
<md5>1a79a4d60de6718e8e5b326e338ae533</md5>
</vulnerability>
<vulnerability title="xss">
<summary>can do xss</summary>
<explanation>xss is bad</explanation>
<recommendation>fix it</recommendation>
<source>users.html</source>
<severity>MEDIUM</severity>
<md5>1a79a4d60de6718e8e5b326e338ae533</md5>
</vulnerability>
</vulnerabilities>
</custom_tool>"""
        expected_result = expected_result.replace("\n", "")

        # when
        save_vulns_to_file([vuln1, vuln2], "/tmp/test.xml", "1.2")

        # then
        with open("/tmp/test.xml") as xml:
            read_data = xml.read()
            self.assertEqual(read_data, expected_result)
