from uleska.formatter import save_vulns_to_file
from uleska.vulnerability import Vulnerability, Severity
from unittest import TestCase


class FormatterTests(TestCase):
    def test_saves_xml_with_one_vuln_and_cvss(self):
        # given
        vuln = Vulnerability(
            "sql injection",
            "can do sql injection",
            "sql injection is bad",
            "fix it",
            "index.html",
            Severity.HIGH,
            cvss="CVSS:3.0/AV:N/AC:H/PR:L/UI:R/S:U/C:L/I:N/A:L",
        )
        expected_result = """<custom_tool schemaVersion="1.1">
<vulnerabilities>
<vulnerability title="sql injection">
<summary>can do sql injection</summary>
<explanation>sql injection is bad</explanation>
<recommendation>fix it</recommendation>
<source>index.html</source>
<severity>HIGH</severity>
<cvss>CVSS:3.0/AV:N/AC:H/PR:L/UI:R/S:U/C:L/I:N/A:L</cvss>
</vulnerability>
</vulnerabilities>
</custom_tool>"""
        expected_result = expected_result.replace("\n", "")

        # when
        save_vulns_to_file([vuln], "/tmp/test.xml", "1.1")

        # then
        with open("/tmp/test.xml") as xml:
            read_data = xml.read()
            self.assertEqual(read_data, expected_result)

    def test_saves_xml_with_one_vuln_and_cve(self):
        # given
        vuln = Vulnerability(
            "sql injection",
            "can do sql injection",
            "sql injection is bad",
            "fix it",
            "index.html",
            Severity.HIGH,
            cve="CVE-2020-1234",
        )
        expected_result = """<custom_tool schemaVersion="1.1">
<vulnerabilities>
<vulnerability title="sql injection">
<summary>can do sql injection</summary>
<explanation>sql injection is bad</explanation>
<recommendation>fix it</recommendation>
<source>index.html</source>
<severity>HIGH</severity>
<cve>CVE-2020-1234</cve>
</vulnerability>
</vulnerabilities>
</custom_tool>"""
        expected_result = expected_result.replace("\n", "")

        # when
        save_vulns_to_file([vuln], "/tmp/test.xml", "1.1")

        # then
        with open("/tmp/test.xml") as xml:
            read_data = xml.read()
            print(read_data)
            self.assertEqual(read_data, expected_result)

    def test_saves_xml_with_one_vuln_and_cwe(self):
        # given
        vuln = Vulnerability(
            "sql injection",
            "can do sql injection",
            "sql injection is bad",
            "fix it",
            "index.html",
            Severity.HIGH,
            cwe="CWE-1234",
        )
        expected_result = """<custom_tool schemaVersion="1.1">
<vulnerabilities>
<vulnerability title="sql injection">
<summary>can do sql injection</summary>
<explanation>sql injection is bad</explanation>
<recommendation>fix it</recommendation>
<source>index.html</source>
<severity>HIGH</severity>
<cwe>CWE-1234</cwe>
</vulnerability>
</vulnerabilities>
</custom_tool>"""
        expected_result = expected_result.replace("\n", "")

        # when
        save_vulns_to_file([vuln], "/tmp/test.xml", "1.1")

        # then
        with open("/tmp/test.xml") as xml:
            read_data = xml.read()
            print(read_data)
            self.assertEqual(read_data, expected_result)
