import argparse
from uleska.vulnerability import Vulnerability, Severity
from uleska.formatter import print_vulns


def main():
    parser = argparse.ArgumentParser(
        description="Takes a vulnerability model and converts it into the correct uleska custom tools format"
    )
    parser.add_argument(
        "-t", "--title", help="Title of the vulnerability", type=str, required=True
    )
    parser.add_argument(
        "-s", "--summary", help="Summary of the vulnerability", type=str, default=""
    )
    parser.add_argument(
        "-e",
        "--explanation",
        help="Explanation of the vulnerability",
        type=str,
        default="",
    )
    parser.add_argument(
        "-r",
        "--recommendation",
        help="Recommendation of the vulnerability",
        type=str,
        default="",
    )
    parser.add_argument(
        "-x",
        "--source",
        help="Source of the vulnerability eg:/index.html or Security.java:line 50",
        type=str,
        default="",
    )
    parser.add_argument(
        "-z",
        "--severity",
        help="Severity of the vulnerability can be HIGH, MEDIUM, LOW, INFORMATIONAL, UNKNOWN",
        type=str,
        default="UNKNOWN",
    )

    parser.add_argument(
        "-cvss", help="Adds a CVSS to vulnerability (version 3 or higher)"
    )
    parser.add_argument("-cve", help="Adds a CVE to vulnerability")
    parser.add_argument("-cwe", help="Adds a CWE to vulnerability")
    parser.add_argument("-md5", help="Adds an md5 to vulnerability")

    args = parser.parse_args()

    severity = Severity.UNKNOWN
    if args.severity == "HIGH":
        severity = Severity.HIGH
    elif args.severity == "MEDIUM":
        severity = Severity.MEDIUM
    elif args.severity == "LOW":
        severity = Severity.LOW
    elif args.severity == "INFORMATIONAL":
        severity = Severity.INFORMATIONAL

    vuln = Vulnerability(
        args.title,
        args.summary,
        args.explanation,
        args.recommendation,
        args.source,
        severity,
    )
    if args.cvss is not None:
        vuln.cvss = args.cvss

    if args.cve is not None:
        vuln.cve = args.cve

    if args.cwe is not None:
        vuln.cwe = args.cwe

    if args.md5 is not None:
        vuln.md5 = args.md5

    print(print_vulns([vuln], "1.2").decode("utf-8"))


if __name__ == "__main__":
    main()
